# ESRIC CLI Tool

ESRIC (Express Service Routing Interface Controller) is a CLI tool to create routes, controllers, interfaces and services for an express server with ease.

# Commands
Run without arguments for Selection

- [server] name
- [service | s] name
- [router | r] name
- [interface | i] name
- [controller | c] name