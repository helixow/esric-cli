package main

import (
	"errors"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/buger/goterm"
	"github.com/pkg/term"
)

// Raw input keycodes
var up byte = 65
var down byte = 66
var escape byte = 27
var enter byte = 13
var keys = map[byte]bool{
	up:   true,
	down: true,
}

type Menu struct {
	Prompt    string
	CursorPos int
	MenuItems []*MenuItem
}

type MenuItem struct {
	Text    string
	ID      string
	SubMenu *Menu
}

func NewMenu(prompt string) *Menu {
	return &Menu{
		Prompt:    prompt,
		MenuItems: make([]*MenuItem, 0),
	}
}

// AddItem will add a new menu option to the menu list
func (m *Menu) AddItem(option string, id string) *Menu {
	menuItem := &MenuItem{
		Text: option,
		ID:   id,
	}

	m.MenuItems = append(m.MenuItems, menuItem)
	return m
}

// renderMenuItems prints the menu item list.
// Setting redraw to true will re-render the options list with updated current selection.
func (m *Menu) renderMenuItems(redraw bool) {
	if redraw {
		// Move the cursor up n lines where n is the number of options, setting the new
		// location to start printing from, effectively redrawing the option list
		//
		// This is done by sending a VT100 escape code to the terminal
		// @see http://www.climagic.org/mirrors/VT100_Escape_Codes.html
		fmt.Printf("\033[%dA", len(m.MenuItems)-1)
	}

	for index, menuItem := range m.MenuItems {
		var newline = "\n"
		if index == len(m.MenuItems)-1 {
			// Adding a new line on the last option will move the cursor position out of range
			// For out redrawing
			newline = ""
		}

		menuItemText := menuItem.Text
		cursor := "  "
		if index == m.CursorPos {
			cursor = goterm.Color("> ", goterm.YELLOW)
			menuItemText = goterm.Color(menuItemText, goterm.YELLOW)
		}

		fmt.Printf("\r%s %s%s", cursor, menuItemText, newline)
	}
}

// Display will display the current menu options and awaits user selection
// It returns the users selected choice
func (m *Menu) Display() string {
	defer func() {
		// Show cursor again.
		fmt.Printf("\033[?25h")
	}()

	fmt.Printf("%s\n", goterm.Color(goterm.Bold(m.Prompt)+":", goterm.CYAN))

	m.renderMenuItems(false)

	// Turn the terminal cursor off
	fmt.Printf("\033[?25l")

	for {
		keyCode := getInput()
		if keyCode == escape {
			return ""
		} else if keyCode == enter {
			menuItem := m.MenuItems[m.CursorPos]
			fmt.Println("\r")
			return menuItem.ID
		} else if keyCode == up {
			m.CursorPos = (m.CursorPos + len(m.MenuItems) - 1) % len(m.MenuItems)
			m.renderMenuItems(true)
		} else if keyCode == down {
			m.CursorPos = (m.CursorPos + 1) % len(m.MenuItems)
			m.renderMenuItems(true)
		}
	}
}

// getInput will read raw input from the terminal
// It returns the raw ASCII value inputted
func getInput() byte {
	t, _ := term.Open("/dev/tty")

	err := term.RawMode(t)
	if err != nil {
		log.Fatal(err)
	}

	var read int
	readBytes := make([]byte, 3)
	read, err = t.Read(readBytes)

	t.Restore()
	t.Close()

	// Arrow keys are prefixed with the ANSI escape code which take up the first two bytes.
	// The third byte is the key specific value we are looking for.
	// For example the left arrow key is '<esc>[A' while the right is '<esc>[C'
	// See: https://en.wikipedia.org/wiki/ANSI_escape_code
	if read == 3 {
		if _, ok := keys[readBytes[2]]; ok {
			return readBytes[2]
		}
	} else {
		return readBytes[0]
	}

	return 0
}

func main() {
	choice := ""
	name := ""

	if len(os.Args) <= 1 {
		menu := NewMenu("What do you want to create?")

		menu.AddItem("Server", "server")
		menu.AddItem("Service", "service")
		menu.AddItem("Route", "route")
		menu.AddItem("Interface", "interface")
		menu.AddItem("Controller", "controller")

		choice = menu.Display()

		if choice != "" && choice != "server" {
			fmt.Println("What's the name of the " + choice + "?")
			fmt.Scanln(&name)

			for len(name) == 0 {
				fmt.Println("What's the name of the " + choice + "?")
				fmt.Scanln(&name)
			}
		}
	} else {
		choice = os.Args[1]
		name = os.Args[2]
	}

	switch choice {
	case "server":
		createServer()
	case "service", "s":
		createService(name)
		return
	case "router", "r", "route":
		createRouter(name)
		return
	case "interface", "i":
		createInterface(name)
		return
	case "controller", "c":
		createController(name)
		return
	default:
		fmt.Println("Unknown command:", choice)
		os.Exit(1)
	}
}

func createFile(folderName string, generationType string, name string) (*os.File, error) {
	path, err := os.Getwd()
	if err != nil {
		fmt.Println("Error getting current workdir", err)
		return nil, err
	}

	if _, err := os.Stat(path + "/" + folderName + "/"); errors.Is(err, os.ErrNotExist) {
		err := os.MkdirAll(path+"/"+folderName+"/", os.ModePerm)
		if err != nil {
			fmt.Println("Error creating "+folderName+" folder", err)
			return nil, err
		}
	}

	var f *os.File

	if len(generationType) > 0 {
		f, err = os.Create(path + "/" + folderName + "/" + name + "." + generationType + ".ts")
	} else {
		f, err = os.Create(path + "/" + folderName + "/" + name + ".ts")
	}

	if err != nil {
		fmt.Println("Error creating file", err)
		return nil, err
	}

	return f, nil
}

func createServer() error {
	fmt.Println("Creating server...")
	f, err := createFile("src", "", "server")

	if err != nil {
		return err
	}

	defer f.Close()

	_, err = f.WriteString(
		`import express from 'express';

const app = express();
const port = 3000;
	
app.listen(port, () => {
	console.log('Server listening on port ' + port + '.');
});
	`)

	return err
}

func createService(name string) (int8, error) {
	fmt.Println("Creating service...")
	f, err := createFile("services", "service", name)

	if err != nil {
		return 0, err
	}

	defer f.Close()

	_, err = f.WriteString("export default class " + name + "Service {\n\tconstructor() { }\n}")

	if err != nil {
		fmt.Println("Error writing file", err)
		return 0, err
	}

	return 1, nil
}

func createRouter(name string) (int8, error) {
	fmt.Println("Creating router...")
	routerFile, err := createFile("routes/"+strings.ToLower(name), "router", strings.ToLower(name))

	if err != nil {
		return 0, err
	}

	defer routerFile.Close()

	controllerFile, err := createFile("routes/"+strings.ToLower(name), "controller", strings.ToLower(name))

	if err != nil {
		return 0, err
	}

	defer controllerFile.Close()

	_, err = routerFile.WriteString(
		`import express from 'express';
import controller from './` + name + `.controller';
		
export default express.Router()
    .get('/', controller.all.bind(controller));
	`)

	if err != nil {
		fmt.Println("Error writing file", err)
		return 0, err
	}

	_, err = controllerFile.WriteString(
		`import { Request, Response } from 'express';
		
export class ` + name + `Controller {
	constructor() { }

	public all(req: Request, res: Response) {
		res.send('Hello World!');
	}
}

export default new ` + name + `Controller();
	`)

	if err != nil {
		fmt.Println("Error writing file", err)
		return 0, err
	}

	return 1, nil
}

func createController(name string) (int8, error) {
	fmt.Println("Creating controller...")
	controllerFile, err := createFile("controllers", "controller", strings.ToLower(name))

	if err != nil {
		return 0, err
	}

	_, err = controllerFile.WriteString(
		`import { Request, Response } from 'express';
		
export class ` + name + `Controller {
	constructor() { }

	public all(req: Request, res: Response) {
		res.send('Hello World!');
	}
}

export default new ` + name + `Controller();
`)

	if err != nil {
		fmt.Println("Error writing file", err)
		return 0, err
	}

	return 1, nil
}

func createInterface(name string) (int8, error) {
	fmt.Println("Creating interface...")
	f, err := createFile("interfaces", "interface", name)

	if err != nil {
		return 0, err
	}

	defer f.Close()

	_, err = f.WriteString("export default interface " + name + " {\n}")

	if err != nil {
		fmt.Println("Error writing file", err)
		return 0, err
	}

	return 1, nil
}
