module esric

go 1.20

require github.com/buger/goterm v1.0.4

require (
	github.com/pkg/term v1.1.0
	golang.org/x/sys v0.0.0-20210331175145-43e1dd70ce54 // indirect
)
