FROM ubuntu:latest AS builder

RUN apt-get update && apt-get install ca-certificates -y && update-ca-certificates
RUN apt install golang-go -y

WORKDIR /app

COPY . .

RUN go build -o ./esric ./src/

FROM scratch

# Copy files
COPY --from=builder ./esric /esric
RUN chmod 777 /esric

# Set working directory
EXPOSE 3000

# Run the application
CMD ["/esric"]